package section4;

public class Q12 {
    public static void main (String args) {
        int[][] i = {{1, 2}, {3, 4}};

        int[][] b = new int[2][2];
        b[0][0] = 3;
        b[0][1] = 3;
        b[1][0] = 3;
        b[1][1] = 3;

        int[][] c = {{1,2},{1,2},{1,2},{4,2}};

        int[][][] e = {{{1,2,3}, {1,2,3}, {1,2,3}}, {{1,2,3}, {1,2,3}, {1,2,3}}, {{1,2,3}, {1,2,3}}};
        //                                      　　↑ここまでで１つの２次元配列
        int[ ][ ][ ] array3D = new int[2][2][2];
        array3D[0][0][0] = 0;
        array3D[0][1][0] = 1;
        array3D[1][0][0] = 2;
        array3D[0][1][0] = 3;
    }
}
