package section5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;

public class Qex1 {
    public static void main(String[] args) {
        List<Integer> a = asList(1, 2);
        List<Integer> x = asList(1, 2);
        int[][] b = {{1, 2}, {3, 4}};
        int[][][] c = {{{1, 2}, {3, 4}}, {{5, 6}, {7, 8}}};
        System.out.println(a.get(0) == 1);
        System.out.println(a.equals(x));
        System.out.println(a == x);
        System.out.println(b[0].equals(a));
        System.out.println(c[0].equals(b));
    }
}
