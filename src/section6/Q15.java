package section6;
import static section6.Int.i;

public class Q15 {
}
class Int {
    static int i = 100;
}

class Main {
    int i;
    public static void main(String args[]) {
        new Main().test();
    }

    public void test() {
        int i = 10;
        this.i = i * 2;
        sum(this.i);
        System.out.println(i);
    }

    static void sum(int i) {
        Int.i += i;
    }
}
