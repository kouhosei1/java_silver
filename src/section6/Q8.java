package section6;

public class Q8 {
    public static void main(String args[]){
        Pug pug = new Pug();
        pug.walk('d');
        pug.walk(0.1);
    }

}

class Pug{
    void walk() {}
    void walk(double d){
        System.out.println(d);
    }
}
