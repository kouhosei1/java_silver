package section7;

public class Q17 {
    public static void main (String args[]){
        Q17 sp = new Tell();
        Tell te = new Tell();
        ((Truth)te).tell();
    }
}
class Tell extends Q17 implements Truth {
    public void tell() {
        System.out.println("Right on!");
    }
}
interface Truth {
    public void tell();
}
