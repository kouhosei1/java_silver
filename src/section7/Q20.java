package section7;

public class Q20 {
    Colibri c;
    void setInstance(Colibri c) {
        this.c = c;
    }
    void twitter() {
        System.out.println(c);
    }
}

class  Colibri extends Q20 {
    @Override
    public String toString() {
        return "Humming";
    }
}

class Main {
    public static void main(String[] args) {
        Colibri colibri = new Colibri();
        Q20 q20 = (Q20)colibri;
        q20.setInstance(colibri);
        q20.twitter();
    }
}
