package section7;

public class Q27 {
    Q27() {
        System.out.print(1);
    }

    Q27(int num) {
        this();
        System.out.print(2);
    }
}
class Tea extends Q27 {
    Tea() {
        super(6);
        System.out.print(3);
    }
    Tea(int num) {
        this();
        System.out.println(4);
    }
    public static void main(String[] args) {
        new Tea(5);
    }
}
