package section7;

public class Q30 {
    public static void main(String[] args) {
        Test2 t2 = new Test2(7);
    }
}
class Test1 {
    public Test1() {
        System.out.print("1 ");
    }
    public Test1(int x){
        System.out.println("因数ありのコンストラクタ");
    }
}
class Test2 extends Test1 {
    public Test2(int x) {
        System.out.print("2 ");
    }
}
