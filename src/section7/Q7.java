package section7;

public class Q7 {
    public void foo(){
        System.out.println("Afoo");
    }
}
class Test extends Q7 {
    public void foo(){
        System.out.println("Bfoo");
    }
    public static void main(String args[]){
        Q7 test0 = new Test();
        Test test = (Test)test0;
        test.foo();
        test0.foo();

    }

}
